import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateMetricsAndMetricValuesTables1705663865681 implements MigrationInterface {
    name = 'CreateMetricsAndMetricValuesTables1705663865681'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "metrics" ("metricId" SERIAL NOT NULL, "name" character varying(200) NOT NULL, "creationDate" TIMESTAMP NOT NULL DEFAULT (NOW() at time zone 'utc'), CONSTRAINT "PK_7c30f94cbd01169f5da32eedb44" PRIMARY KEY ("metricId"))`);
        await queryRunner.query(`CREATE TABLE "metric_values" ("valueId" SERIAL NOT NULL, "metricId" integer NOT NULL, "value" numeric NOT NULL, "timestamp" TIMESTAMP NOT NULL DEFAULT (NOW() at time zone 'utc'), CONSTRAINT "PK_a0846d233dc4d290182e2a4db85" PRIMARY KEY ("valueId"))`);
        await queryRunner.query(`ALTER TABLE "metric_values" ADD CONSTRAINT "FK_f11956e5c13c2185591b8a75844" FOREIGN KEY ("metricId") REFERENCES "metrics"("metricId") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "metric_values" DROP CONSTRAINT "FK_f11956e5c13c2185591b8a75844"`);
        await queryRunner.query(`DROP TABLE "metric_values"`);
        await queryRunner.query(`DROP TABLE "metrics"`);
    }

}
