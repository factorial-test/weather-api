
# Wheather API

Wheather API is a service that collects, stores, and prepares weather metrics for visualization. 

### Quick links

- [API Doc (Swagger)](https://weather-api-factorial-bcfe94683491.herokuapp.com/api)
- [Try the App](https://wheather-front.vercel.app/)

### The structure

The codebase is split into modules following DDD approach. There are 3 modules:
- Metric - handles the logic of operations under metrics and their time values;
- Auth - responsible for authorization
- App Module - incapsuletes Metric and Auth modules into a single Nest Application

Each module is devided into controllers and services, where controllers are responsible for communication with extrenal clients, and services handle the business logic. Repositories are used by services for interaction with the database.

The structure of routes follows **REST Api design** best practices. GET routes are open for using without authorization, and POST/PUT/DELETE routes require the authorization.

The database schema contains 3 tables:
- metrics - list of all metrics
- metric_values - values of each metric linked using one-to-money relation by metricId
- migrations - system table that tracks processed db schema migrations

### Key decisions

- To use SQL database and calculate averages on the backend side. Advantages: fast calculations, less data is transferred between server and client, it's possible to cache same requests across all clients.
- To use authorization and Auth guards to protect data from unauthorized modification and risk of loss. For simiplicity, local strategy was used to exchange super admin's login & password for a JWT token.
- Using of Typescript for strict typings. Advantages: maintainabilty, scalability, auto-generation of Swagger docs.
- Using database migrations for controlling schema updates. Migrations are generated by cli command using Entity-first approach that checks for the Models updates.
- POST /api/metrics/{id}/values takes array of values that simplifies batch updates.

### Possible improvements

- To implement unit and e2e tests.
- To optimize database for select quiries by adding index to metric_values.timestamp field.
- To cache results of GET /metrics/{id}/values requests having the same parameters.
- To use more secure authorization strategy such as Oauth 2.0. Implement User service with access control functionality.
- To provide for the database sharding and replication strategy. If we track every metric on minute basis, then we need 1GB/city/metric/year. For 1k cities and 20 metrics it equals 20PB/year.
- To add validation of request params and use 400 response code.
- To use tracing and logging systems such as GreyLog, Grafana, Sentry.
- RESTful is partially implemented and response structure is not consistent. Also the API Docs only describes 200 responses and doesn't have all schema types and examples.

### Get Started

- install dependencies `yarn`
- setup .env file in root directory:

```
DB_HOST=
DB_PORT=
DB_USERNAME=
DB_PASSWORD=
DB_NAME=
JWT_SECRET=
ADMIN_USERNAME=
ADMIN_PASS=
```

- run migrations: `yarn migrate:up`
- start the app: `yarn start:dev`
