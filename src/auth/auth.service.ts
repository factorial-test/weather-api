
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { createHash } from 'crypto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
  ) { }

  async login(username: string, pass: string): Promise<{ accessToken: string }> {
    const passHash = createHash('md5').update(pass).digest("hex");
    if (username !== process.env.ADMIN_USERNAME || passHash !== process.env.ADMIN_PASS) {
      throw new UnauthorizedException()
    };

    return {
      accessToken: this.jwtService.sign({ username })
    }
  }
}