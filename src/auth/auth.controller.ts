import { Controller, Request, Post, UseGuards, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { LoginDTO } from './dto/login.dto';

@Controller('/api/auth')
@ApiTags('Auth')
export class AuthController {
  constructor(
    private authService: AuthService
  ) { }

  @Post('/login')
  
  @ApiResponse({ status: 201, description: "returns accessToken on success" })
  async login(
    @Body() { username, password }: LoginDTO,
  ): Promise<{ accessToken: string }> {
    return this.authService.login(username, password);
  }
}