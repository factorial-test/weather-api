import { Module } from '@nestjs/common';
import { MetricController } from './metric.controller';
import { MetricService } from './metric.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Metric } from './entity/metric.entity';
import { MetricValue } from './entity/metricValue.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Metric, MetricValue])
  ],
  controllers: [MetricController],
  providers: [MetricService],
})
export class MetricModule {}
