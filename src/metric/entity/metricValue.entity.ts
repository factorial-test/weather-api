import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Metric } from "./metric.entity";

@Entity('metric_values')
export class MetricValue {
  @PrimaryGeneratedColumn({ type: 'int' })
  valueId: number;

  @Column({ type: 'int' })
  metricId: number;

  @Column({ type: 'decimal' })
  value: number;

  @Column({ type: 'timestamp without time zone', default: () => "(NOW() at time zone 'utc')" })
  timestamp: Date;

  @ManyToOne(() => Metric, (metric) => metric.values, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'metricId' })
  metric: Metric;
}
