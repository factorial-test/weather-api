import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { MetricValue } from "./metricValue.entity";
import { ApiProperty } from "@nestjs/swagger";

@Entity('metrics')
export class Metric {
  @PrimaryGeneratedColumn({ type: 'int' })
  @ApiProperty()
  metricId: number;

  @Column({ type: 'varchar', length: 200 })
  @ApiProperty()
  name: string;

  @Column({ type: 'timestamp without time zone', default: () => "(NOW() at time zone 'utc')" })
  @ApiProperty()
  creationDate: Date;

  @OneToMany(() => MetricValue, (metricValue) => metricValue.metric)
  values: MetricValue[]
}
