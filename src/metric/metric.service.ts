import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Metric } from "./entity/metric.entity";
import { Between, Repository } from "typeorm";
import { MetricValue } from "./entity/metricValue.entity";

export enum TimeIntevalEnum {
  minute = 'minute',
  hour = 'hour',
  day = 'day',
}

@Injectable()
export class MetricService {
  constructor(
    @InjectRepository(Metric)
    private _metricRepo: Repository<Metric>,

    @InjectRepository(MetricValue)
    private _metricValueRepo: Repository<MetricValue>,
  ) { }

  async listMetrics(): Promise<Metric[]> {
    return this._metricRepo.find();
  }

  async getMetricById(metricId: number) {
    return this._metricRepo.findOneBy({ metricId });
  }

  async createMetric(metric: Omit<Metric, 'metricId'>): Promise<Metric> {
    return await this._metricRepo.save(metric);
  }

  async updateMetric(metricId: number, metricData: Partial<Metric>): Promise<Metric> {
    await this._metricRepo.update({ metricId }, metricData);
    return this.getMetricById(metricId);
  }

  async deleteMetric(metricId: number): Promise<boolean> {
    await this._metricRepo.delete({ metricId });
    return true;
  }

  async addMetricValues(metricId: number, values: Omit<MetricValue, 'valueId' | 'metricId'>[]): Promise<number> {
    const valuesToInsert = values.map(value => ({ ...value, metricId }));
    await this._metricValueRepo.insert(valuesToInsert);
    return values.length;
  }

  /**
   * Retrieves metric's values and calculates averages for provided interval length.
   * @param metricId ID of a metric which values should be retrieved
   * @param from timestamp of beginning of the period (inclusive)
   * @param to timestamp of the end of the period (inclusive)
   * @param interval length of minimum step, may take values: minute, day or hour
   * @returns ordered array of (date-string, value) tuples 
   */
  async getMetricValues(metricId: number, from: Date, to: Date, interval: TimeIntevalEnum): Promise<[string, number][]> {
    let dateFormat = 'YYYY-MM-DD';
    if (interval === TimeIntevalEnum.hour) {
      dateFormat += ' HH24:00:00';
    } else if (interval === TimeIntevalEnum.minute) {
      dateFormat += ' HH24:MI:00'
    }

    const records = await this._metricValueRepo
      .createQueryBuilder()
      .select(`TO_CHAR(timestamp, '${dateFormat}') AS time_interval`)
      .addSelect('ROUND(AVG(value)::numeric, 2) as average_value')
      .where({ metricId, timestamp: Between(from, to) })
      .groupBy('time_interval')
      .orderBy('time_interval')
      .getRawMany<{ time_interval: string, average_value: string }>();

    return records.map(r => [r.time_interval, Number(r.average_value)]);
  }
}