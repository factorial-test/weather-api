import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { MetricService, TimeIntevalEnum } from './metric.service';
import { Metric } from './entity/metric.entity';
import { MetricValue } from './entity/metricValue.entity';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller('/api/metrics')
@ApiTags('Metrics')
export class MetricController {
  constructor(private readonly metricsService: MetricService) {}

  @Get()
  @ApiResponse({ type: Metric, isArray: true })
  listMetrics(): Promise<Metric[]> {
    return this.metricsService.listMetrics();
  }

  @Get('/:id')
  @ApiResponse({ type: Metric })
  getMetric(@Param('id') metricId: number): Promise<Metric> {
    return this.metricsService.getMetricById(metricId);
  }

  @Post()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiResponse({ type: Metric })
  createMetric(@Body() metric: Metric): Promise<Metric> {
    return this.metricsService.createMetric(metric);
  }

  @Put('/:id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiResponse({ type: Metric })
  updateMetric(@Param('id') metricId: number, @Body() metric: Metric): Promise<Metric> {
    return this.metricsService.updateMetric(metricId, metric);
  }

  @Delete('/:id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiResponse({ status: 200, description: "returns true on success"})
  async deleteMetric(@Param('id') metricId: number): Promise<{ success: boolean}> {
    const result = await this.metricsService.deleteMetric(metricId);
    return {
      success: result,
    }
  }

  @Get('/:id/values')
  @ApiResponse({ status: 201, description: "returns ordered array of [date, value] tuples"})
  listMetricValues(
    @Param('id') metricId: number,
    @Query('from') from: Date,
    @Query('to') to: Date,
    @Query('interval') interval: TimeIntevalEnum,
  ) {
    return this.metricsService.getMetricValues(metricId, from, to, interval); 
  }

  @Post('/:id/values')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiResponse({ status: 200, description: "returns number of inserted records"})
  async createMetricValue(@Param('id') metricId: number, @Body() values: MetricValue[]): Promise<{ inserted: number }> {
    const result = await this.metricsService.addMetricValues(metricId, values);
    return {
      inserted: result,
    }
  }
}
