import { DataSource, DataSourceOptions } from 'typeorm';
import * as dotenv from 'dotenv';
import { join } from 'path';
dotenv.config();

export const dataSourceOptions: DataSourceOptions = {
  type: 'postgres',
  host: process.env.DB_HOST || 'localhost',
  port: Number(process.env.DB_PORT || 5432),
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  synchronize: false,
  entities: [join(__dirname, '**', '*.entity.{ts,js}')],
  migrations: [join(__dirname, '../migrations/*.ts')],
  migrationsTableName: "migrations",
  ...(process.env.NODE_ENV === 'production' ? {
    ssl: {
      rejectUnauthorized: false
    }
  } : {})
}

export const dataSource = new DataSource(dataSourceOptions);